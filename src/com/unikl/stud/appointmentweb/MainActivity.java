package com.unikl.stud.appointmentweb;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MainActivity extends Activity {
	private WebView webViewMain;
	private ProgressDialog progressDialog;
//	private String receiverMail;
	private SharedPreferences spPreferences;
	private String emailString;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
    }
    

	@Override
	protected void onStart() {
		
		super.onStart();
		setContentView(R.layout.activity_main);
        spPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        emailString = spPreferences.getString("email", "contact@awislabs.com");
        progressDialog = new ProgressDialog(this);
        
        progressDialog.setMessage("Loading..Please wait");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
        webViewMain = (WebView) findViewById(R.id.webViewMain);
        webViewMain.loadUrl("http://awislabs.com/project/appointmentWeb/index.php?email="+emailString);
        
        webViewMain.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url){
                // do your handling codes here, which url is the requested url
                // probably you need to open that url rather than redirect:
                view.loadUrl(url);
                return false; // then it is not handled by default action
           }
        });
        
        webViewMain.setWebChromeClient(new WebChromeClient() {
        	public void onProgressChanged(WebView view, int progress){      
        		progressDialog.setProgress(progress);
        		
            	if (progress == 100){
            		progressDialog.dismiss();          		
            	} else progressDialog.show();
            }
            });
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_settings:
			Intent i = new Intent(this, Preferences.class);
			startActivity(i);
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
